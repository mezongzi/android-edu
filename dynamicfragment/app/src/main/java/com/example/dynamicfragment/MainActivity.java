package com.example.dynamicfragment;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * 入口文件啊
 * @author 粽子
 */
public class MainActivity extends FragmentActivity implements View.OnClickListener {
    FrameLayout frameLayout;
    TextView tvHome;
    TextView tvWechat;
    TextView tvMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //视图
        this.initView();

        //默认
        this.changeFrame(new HomeFragment(),"HomeFragment");
    }

    public void initView(){
        //找控件
        frameLayout = findViewById(R.id.mainFrame);
        tvHome = findViewById(R.id.tv_home);
        tvWechat = findViewById(R.id.tv_wechat);
        tvMe = findViewById(R.id.tv_me);

        //绑定事件
        tvHome.setOnClickListener(this);
        tvWechat.setOnClickListener(this);
        tvMe.setOnClickListener(this);
    }

    /**
     * 点击切换效果事件
     * @param view
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_home:
                this.changeFrame(new HomeFragment(),"HomeFragment");
                break;
            case R.id.tv_wechat:
                this.changeFrame(new WechatFragment(),"WechatFragment");
                break;
            case R.id.tv_me:
                this.changeFrame(new MeFragment(),"MeFragment");
                break;
            default:
                break;
        }
    }

    /**
     * 切换啦
     * @param fragment
     * @param tag
     */
    public void changeFrame(Fragment fragment,String tag){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame,fragment,tag);
        fragmentTransaction.commit();
    }
}