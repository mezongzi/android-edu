package com.example.dynamicfragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * 主页的fragment
 * @author 粽子
 */
public class HomeFragment extends Fragment {
    private TextView homeText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment,container,false);
        homeText = view.findViewById(R.id.homeText);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        homeText.setText("哈哈哈哈，我改成了主页的文字哦");
    }
}
