package com.example.viewpagefragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * 入口页面
 * @author 粽子
 */
public class MainActivity extends FragmentActivity implements View.OnClickListener {
    private ViewPager viewPager;
    private TextView tvCurrent;
    private TextView tvLate;
    ArrayList<TextView> tvTabs = new ArrayList<TextView>();

    /**
     * 创建啊
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //设置视图
        initView();

        //设置事件
        initEvent();
    }

    /**
     * 展示视图啊
     * @author 粽子
     */
    public void initView(){
        //查找组件
        tvCurrent = findViewById(R.id.tv_current);
        tvLate = findViewById(R.id.tv_late);
        viewPager = findViewById(R.id.viewPager);

        //加入到list
        tvTabs.add(tvCurrent);
        tvTabs.add(tvLate);
    }

    /**
     * 设置事件
     * @author 粽子
     */
    public void initEvent(){
        //绑定事件
        tvCurrent.setOnClickListener(this);
        tvLate.setOnClickListener(this);

        //设置适配器
        viewPager.setAdapter(this.setFragmentPagerAdapter());

        //切换的效果
        viewPager.addOnPageChangeListener(this.addOnPageChangeListener());
    }

    /**
     * 添加点击事件
     * @param view
     * @author 粽子
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_current:
                viewPager.setCurrentItem(0);
                break;
            case R.id.tv_late:
                viewPager.setCurrentItem(1);
                break;
        }
    }

    /**
     * 开始处理viewpager的事件啊
     * @return FragmentPagerAdapter
     * @author 粽子
     */
    public FragmentPagerAdapter setFragmentPagerAdapter(){
        return new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                Fragment fragment = new Fragment();
                switch (position){
                    case 0:
                        fragment = new CurrentFragment();
                        break;
                    case 1:
                        fragment = new LateFragment();
                        break;
                }
                return fragment;
            }

            @Override
            public int getCount() {
                return 2;
            }
        };
    }

    /**
     * 绑定滑动的事件
     * @author 粽子
     */
    public ViewPager.OnPageChangeListener addOnPageChangeListener(){
        return new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float v, int i1) {
                for (int i = 0; i < tvTabs.size(); i++) {
                    if(i == position){
                        tvTabs.get(i).setTextColor(Color.RED);
                    }else{
                        tvTabs.get(i).setTextColor(Color.BLACK);
                    }
                }
            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        };
    }
}
