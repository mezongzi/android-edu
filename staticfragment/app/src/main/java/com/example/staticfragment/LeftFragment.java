package com.example.staticfragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;

/**
 * 左边的fragment页面
 * @author 粽子
 */
public class LeftFragment extends Fragment {
    private ListView listView;
    private List<String> datalist;

    /**
     * 创建视图
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_fragment,container,false);
        listView = view.findViewById(R.id.listView);
        return view;
    }

    /**
     * 创建acitivty成功之后
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //初始化数据
        initData();

        //设置数据
        listView.setAdapter(new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,datalist));
    }

    /**
     * 初始化数据
     * @author 粽子
     */
    private void initData() {
        datalist = new ArrayList<String>();
        for (int i = 0; i < 20 ; i++) {
            datalist.add("这个left_fragment是模拟的数据"+i);
        }
    }
}